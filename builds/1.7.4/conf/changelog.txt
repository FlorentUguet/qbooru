Changelog :

	Viewer Version 1.5.0
		+ Added Danbooru support
		+ Added a window to show you what booru is loading, at startup. Still in beta
		~ Lots of variable cleaning. Lots of enhancements with std::strings

	Viewer Version 1.3.2
		+ Log behaviour enhanced. More readable, and cleaned a lot

	Viewer Version 1.3.1
		+ You can now define a default search rating in the config file

	Viewer Version 1.3.0
		+ Built under Qt 5.3, you need to DL the full installer at : http://www.aht.li/2526278/QBooruSetup.exe
	Updater Version 1.0.0
		~ Built under Qt 5.3, you need to DL the full installer at : http://www.aht.li/2526278/QBooruSetup.exe

	Viewer Version 1.2.5
		+ Added a check that generates a default config file if needed

	Viewer Version 1.2.4
		+ Fixed all the actions coming from the Viewer tab

	Viewer Version 1.2.3
		+ Now you can add/delete/modify boorus based on Gelbooru/Moebooru

	Viewer Version 1.2.0
		+ Started a new fully modulable way of adding Boorus
		+ Lots of adds in Configuration files
		+ More complete program log

	Viewer Version 1.1.7
		~ Window name now shows App Version
	Updater Version 0.1.10
		~ Window name now shows App Version
		~ Attempted to fix a bug causing the program not to update the updater's version

	Viewer Version 1.1.6
		~ Fixed Gelbooru log messages not including date

	Viewer Version 1.1.5
		~ Fixed Gelbooru skipping the 1st search page, indexed as 0

	Viewer Version 1.1.4
		~ Fixed a crash caused by pictures with more than 256 tags

	Viewer Version 1.1.2
		~ Finally fixed cache not clearing

	Viewer Version 1.1.0
		+ Added proper config file
		+ Added proper log file
		~ Fixed post's date on Viewer
		~ Cleaning the output-related code
		~ Lots of minor fixes and improvements

	Viewer Version 1.0.2
		+ Better layout on search page
		+ Viewer now adapt the medium preview with the window's size
		+ Fixed cache not always clearing
		+ Some cleaning in sub-classes

	Viewer Version 1.0.1
		+ Arrow keys support on Viewer Tab
		+ Reimplented switching between pictures from Viewer Tab
		+ Fixed Cache size not showing
		+ Added loading items to the Viewer

	Viewer Version 1.0.0
		+ App is now QBooru :D
		+ Completely reworked code. Went from QT Designer to code for UI
		+ Added a fullscreen viewer on the Viewer Tab
		+ Lightened the UI and the loading times
	Updater Version 0.1.7
		+ Reset links and file names for updating